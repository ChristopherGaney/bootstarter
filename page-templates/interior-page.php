<?php
/*
Template Name: Interior Page
*/

get_header(); 

?>

<!--<section class="breadcrumb_section">
    <div class="breadcrumbs">
       <?php
            //if ( function_exists('yoast_breadcrumb') ) {
             // yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
           // }
        ?> 
    </div>
</section>-->


<?php

        if( have_rows('universal_fields') ):
    
            while ( have_rows('universal_fields') ) : the_row();

                if( get_row_layout() == 'page_builder' ):
 
                    get_template_part( 'template-parts/page-builder' );

                elseif( get_row_layout() == 'products' ):
 
                    get_template_part( 'template-parts/products' );

                elseif( get_row_layout() == 'infinite_ticker' ):
 
                    get_template_part( 'template-parts/infinite-ticker' );

                endif;

            endwhile;

        endif;

    ?>  
    


<?php get_footer();

