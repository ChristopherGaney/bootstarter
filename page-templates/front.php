<?php
/*
Template Name: Front
*/

get_header(); ?>


<?php

        if( have_rows('universal_fields') ):
    
            while ( have_rows('universal_fields') ) : the_row();

                if( get_row_layout() == 'page_builder' ):
 
                    get_template_part( 'template-parts/page-builder' );

                elseif( get_row_layout() == 'products' ):
 
                    get_template_part( 'template-parts/products' );

                elseif( get_row_layout() == 'infinite_ticker' ):
 
                    get_template_part( 'template-parts/infinite-ticker' );

                endif;

            endwhile;

        endif;

    ?>  
    


<?php get_footer();

