<?php
				// If Single or Archive (Category, Tag, Author or a Date based page).
				if ( is_single() || is_archive() ) :
			?>
					</div><!-- /.col -->

					<?php
						get_sidebar();
					?>

				</div><!-- /.row -->
			<?php
				endif;
			?>
		</main><!-- /#main -->
		<footer id="footer" class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<a class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
							<?php
								$header_logo = get_theme_mod( 'header_logo' ); // Get custom meta-value.

								if ( ! empty( $header_logo ) ) :
							?>
								<img src="<?php echo esc_url( $header_logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
							<?php
								else :
									echo esc_attr( get_bloginfo( 'name', 'display' ) );
								endif;
							?>
						</a>
					</div>
					<div class="col-md-8 nav_wrap">
					<?php
						if ( has_nav_menu( 'footer-menu' ) ) : // See function register_nav_menus() in functions.php
							/*
								Loading WordPress Custom Menu (theme_location) ... remove <div> <ul> containers and show only <li> items!!!
								Menu name taken from functions.php!!! ... register_nav_menu( 'footer-menu', 'Footer Menu' );
								!!! IMPORTANT: After adding all pages to the menu, don't forget to assign this menu to the Footer menu of "Theme locations" /wp-admin/nav-menus.php (on left side) ... Otherwise the themes will not know, which menu to use!!!
							*/
							wp_nav_menu(
								array(
									'theme_location'  => 'footer-menu',
									'container'       => 'nav',
									'container_class' => 'nav_list',
									'fallback_cb'     => '',
									'items_wrap'      => '<ul class="menu nav justify-content-end">%3$s</ul>',
									//'fallback_cb'    => 'WP_Bootstrap4_Navwalker_Footer::fallback',
									'walker'          => new WP_Bootstrap4_Navwalker_Footer(),
								)
							);
						endif; ?>
						<div class="commerce">
							<i class="material-icons">person</i>
							<i class="material-icons">shopping_cart</i>
						</div>
					</div>
						<div class="col-md-12 social_search">
							<div class="container">
								<form class="search-form my-2 my-lg-0" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
									<div class="input-group mb-3">
										<input type="text" name="s" class="form-control" placeholder="<?php esc_attr_e( 'Search', 'my-theme' ); ?>" title="<?php esc_attr_e( 'Search', 'my-theme' ); ?>" />
										<button type="submit" name="submit" class="btn btn-outline-secondary"><i class="material-icons" style="font-size:24px;color:<?php echo $primary_color; ?>">search</i></button>
									</div>
								</form>
								<?php 
								if ( is_active_sidebar( 'secondary_widget_area' ) ) {
									dynamic_sidebar( 'secondary_widget_area' );
								}
				
								?>
							
							</div>
						</div>
						<div class="col-md-12 copyright">
							<div class="terms">
								<a href="<?php echo esc_url( home_url() ); ?>/privacy-policy">Privacy Policy</a><a href="<?php echo esc_url( home_url() ); ?>/terms-and-conditions">Terms and Conditons</a>
							</div>
							<p><?php printf( esc_html__( '&copy; %1$s %2$s.', 'my-theme' ), date_i18n( 'Y' ), get_bloginfo( 'name', 'display' ) ); ?></p>
						</div>

						<?php if ( is_active_sidebar( 'third_widget_area' ) ) : ?>
						<div class="col-md-12">
							<?php
								dynamic_sidebar( 'third_widget_area' );

								if ( current_user_can( 'manage_options' ) ) :
							?>
								<span class="edit-link"><a href="<?php echo esc_url( admin_url( 'widgets.php' ) ); ?>" class="badge badge-secondary"><?php esc_html_e( 'Edit', 'my-theme' ); ?></a></span><!-- Show Edit Widget link -->
							<?php
								endif;
							?>
						</div>
					<?php
						endif;
					?>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</footer><!-- /#footer -->
	</div><!-- /#wrapper -->
	<?php
		wp_footer();
	?>
</body>
</html>