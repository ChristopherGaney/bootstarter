var responsiveImage = (function() {
	var imgs = document.querySelectorAll('[data-back]');
	var w = window.innerWidth;

	var run = function() {
		imgs.forEach(function(el,i) {
			var urls = el.getAttribute('data-back').split('|');
			var src = '';

			if(w >= 992) {
				src = urls[1];
			}
			else {
				src = urls[0];
			}
			if(el.tagName === 'IMG') {
				el.src = src;
			}
			else {
				el.style.backgroundImage = 'url(' + src + ')';
			}
		});
	}

	run();

	return {
		resize: function(width) {
			if((width >= 992 && w < 992) || (width < 992 && w >= 992)) {
				w = width;
				run();
			}
		}
	};
})();

module.exports = responsiveImage;


