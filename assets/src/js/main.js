
// Webpack Imports
import * as bootstrap from 'bootstrap';
//import responsiveImage from './lib/responsiveImage.js';


( function () {
	'use strict';

	// Focus input if Searchform is empty
	[].forEach.call( document.querySelectorAll( '.search-form' ), ( el ) => {
		el.addEventListener( 'submit', function ( e ) {
			var search = el.querySelector( 'input' );
			if ( search.value.length < 1 ) {
				e.preventDefault();
				search.focus();
			}
		} );
	} );

	// Initialize Popovers: https://getbootstrap.com/docs/5.0/components/popovers
	var popoverTriggerList = [].slice.call( document.querySelectorAll( '[data-bs-toggle="popover"]' ) );
	var popoverList = popoverTriggerList.map( function ( popoverTriggerEl ) {
		return new bootstrap.Popover( popoverTriggerEl, {
			trigger: 'focus',
		} );
	} );

	/*****************************************************************************
        Debounce
    *****************************************************************************/
        function debounce(func, wait, immediate) {
            var timeout;
                return function() {
                    var context = this, args = arguments;
                    var later = function() {
                        timeout = null;
                        if (!immediate) func.apply(context, args);
                    };
                    var callNow = immediate && !timeout;
                    clearTimeout(timeout);
                    timeout = setTimeout(later, wait);
                    if (callNow) func.apply(context, args);
                };
            };
    /*****************************************************************************
        End Debounce
    *****************************************************************************/


	 /*****************************************************************************
        Alert Subscribers when the screenwidth changes 
    *****************************************************************************/
    var screenWidth = (function() {
        var callDeBounce = debounce(function() {
            screenWidth.resizeWidth();
        }, 250);
        
        window.onresize = function() {
            callDeBounce();
        };
        /* This Subscriber function will call any listed functions
        that need to know about the resize event*/
        var alertSubscribers = function(w) {
            /* add subscribers here */
                //responsiveImage.resize(w);
        };
        
        return {
                getWidth: function() {
                    return window.innerWidth;
                },
                resizeWidth: function() {
                    alertSubscribers(window.innerWidth);
                }
            };
    })(); 
    
    /*****************************************************************************
        End Screenwidths
    *****************************************************************************/  

    /*****************************************************************************
        Toggle Off Canvas
    *****************************************************************************/
        var offCanvas = (function() {
            var active = false;
            var el = $('#toggle-nav');
            el.click(function(e) {
                if(!active) {
                    el.addClass('active');
                    active = true;
                }
                else {
                    el.removeClass('active');
                    el.addClass('not-active');
                    setTimeout(function() {
                        el.removeClass('not-active');
                    },500);
                    active = false;
                }
            });
        })();
    /*****************************************************************************
        End Toggle Off Canvas
    *****************************************************************************/

    /*****************************************************************************
        Hero Animations
    *****************************************************************************/
        var heroAnimate = (function() {
            var priceVisible = false;
            var textVisible = false;
            

            var priceAnimate = function() {
                var vals = $('.price_val');
                var ease = [.18, .14, .12, .1];
                var x = 0;
                $.each(vals, function(i, el) {
                    x = x + ease[i] * 1000;
                    setTimeout(function() {
                        $(el).addClass('vis');
                    }, x);
                });
            };

            var textAnimate = function() {
                setTimeout(function() {
                    $('.service_hero .inside_box').addClass('vis');
                }, 300);
            };
            var debounceScroll = debounce(function() {
                if(priceVisible === false || textVisible === false) {
                    var width = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                    var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
                    if(width < 768) {
                        if(scrollTop > 850 && scrollTop < 1200 && priceVisible === false) {
                            priceAnimate();
                            priceVisible = true;
                        }
                        if(scrollTop > 1100 && scrollTop < 1500 && textVisible === false) {
                            textAnimate();
                            textVisible = true;
                        }
                    } 
                    else if(width >= 768 && width < 992) {
                        if(scrollTop > 350 && scrollTop < 830) {
                            priceAnimate();
                            textAnimate();
                            priceVisible = true;
                            textVisible = true;
                        }
                    }
                    else {
                        if(scrollTop > 170 && scrollTop < 480) {
                            priceAnimate();
                            textAnimate();
                            priceVisible = true;
                            textVisible = true;
                        }
                    }
                }
                else {
                    window.removeEventListener('scroll', debounceScroll);
                }
            }, 150);

            window.addEventListener('scroll', debounceScroll);
        })();
        
        //closet();
        // const observer = new IntersectionObserver(entries => {
        //     entries.forEach(entry => {
        //       if (entry.isIntersecting) {
        //         priceAnimate();
        //       }
        //     });
        // },
        // {threshold: .85});

        // const textserver = new IntersectionObserver(entries => {
        //     entries.forEach(entry => {
        //       if (entry.isIntersecting) {
        //         textAnimate();
        //       }
        //     });
        // },
        // {threshold: .85});

    
        //observer.observe(document.querySelector('.price_block'));

        //textserver.observe(document.querySelector('.service_hero .inside_box'));
    /*****************************************************************************
        End Hero Animations
    *****************************************************************************/
   
} )();






//////////////////