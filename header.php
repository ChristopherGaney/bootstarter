<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php wp_head(); ?>
</head>


<?php
	$navbar_scheme   = get_theme_mod( 'navbar_scheme', 'navbar-light' ); // Get custom meta-value.
	$navbar_position = get_theme_mod( 'navbar_position', 'static' ); // Get custom meta-value.

	$search_enabled  = get_theme_mod( 'search_enabled', '1' ); // Get custom meta-value.

	$primary_color = get_field('site_primary_color', 'option');
	$top_bar = get_field('top_bar', 'option');
	$bottom_bar = get_field('bottom_bar', 'option');

?>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<a href="#main" class="visually-hidden-focusable"><?php esc_html_e( 'Skip to main content', 'my-theme' ); ?></a>

<div id="wrapper" class="site_wrap">
	<header class="site_header">
		<?php if(!empty($top_bar)) : ?>
			<div class="top_bar">
				<div class="container">
					<?php 
					if ( is_active_sidebar( 'secondary_widget_area' ) ) {
						dynamic_sidebar( 'secondary_widget_area' );
					}
	
					?>
					<div class="commerce">
						<a href="<?php echo esc_url( home_url() ); ?>/account/">
							<i class="material-icons" style="font-size:24px;color:#ffffff">person</i>
						</a>
						<a href="<?php echo esc_url( home_url() ); ?>/cart/">
							<i class="material-icons" style="font-size:24px;color:#ffffff">shopping_cart</i>
						</a>
					</div>
				
				</div>
			</div>
		<?php endif; ?>
		<nav id="header" class="navbar navbar-expand-md <?php echo esc_attr( $navbar_scheme ); if ( isset( $navbar_position ) && 'fixed_top' === $navbar_position ) : echo ' fixed-top'; elseif ( isset( $navbar_position ) && 'fixed_bottom' === $navbar_position ) : echo ' fixed-bottom'; endif; if ( is_home() || is_front_page() ) : echo ' home'; endif; ?>">
			<div class="container">
				<a class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php
						$header_logo = get_theme_mod( 'header_logo' ); // Get custom meta-value.

						if ( ! empty( $header_logo ) ) :
					?>
						<img src="<?php echo esc_url( $header_logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
					<?php
						else :
							echo esc_attr( get_bloginfo( 'name', 'display' ) );
						endif;
					?>
				</a>
				<!-- <div class="hamburger_button">
	                        <div class="button_container" id="toggle" data-toggle="offCanvas">
	                            <span class="top"></span>
	                            <span class="middle"></span>
	                            <span class="bottom"></span>
	                        </div>
	                    </div>      -->
				<button class="navbar-toggler" id="toggle-nav" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'my-theme' ); ?>">
					<span class="top"></span>
	                <span class="middle"></span>
	                <span class="bottom"></span>
				</button>

				<div id="navbar" class="collapse navbar-collapse">
					<?php
						// Loading WordPress Custom Menu (theme_location).
						wp_nav_menu(
							array(
								'theme_location' => 'main-menu',
								'container'      => '',
								'menu_class'     => 'navbar-nav ms-auto justify-content-end',
								'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
								'walker'         => new WP_Bootstrap_Navwalker(),
							)
						);

						if ( '1' === $search_enabled ) :
					?>
							<form class="search-form my-2 my-lg-0" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<div class="input-group">
									<input type="text" name="s" class="form-control" placeholder="<?php esc_attr_e( 'Search', 'my-theme' ); ?>" title="<?php esc_attr_e( 'Search', 'my-theme' ); ?>" />
									<button type="submit" name="submit" class="btn btn-outline-secondary"><i class="material-icons" style="font-size:24px;color:<?php echo $primary_color; ?>">search</i></button>
								</div>
							</form>
					<?php
						endif;
					?>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container -->
		</nav><!-- /#header -->
		<?php if(!empty($bottom_bar)) : ?>
			<div class="bottom_bar">
				<div class="container">
					<?php 
					if ( is_active_sidebar( 'secondary_widget_area' ) ) {
						dynamic_sidebar( 'secondary_widget_area' );
					}
	
					?>
					<div class="commerce">
						<a href="<?php echo esc_url( home_url() ); ?>/account/">
							<i class="material-icons">person</i>
						</a>
						<a href="<?php echo esc_url( home_url() ); ?>/cart/">
							<i class="material-icons">shopping_cart</i>
						</a>
					</div>
				
				</div>
			</div>
		<?php endif; ?>
	</header>


	<main class="main"<?php if ( isset( $navbar_position ) && 'fixed_top' === $navbar_position ) : echo ' style="padding-top: 100px;"'; elseif ( isset( $navbar_position ) && 'fixed_bottom' === $navbar_position ) : echo ' style="padding-bottom: 100px;"'; endif; ?>>
	 	<?php
			// If Single or Archive (Category, Tag, Author or a Date based page).
			if ( is_single() || is_archive() ) :
		?>
			<div class="row chub">
				<div class="col-md-8 col-sm-12">
		<?php
			endif;
		?>
