## bootStarter

bootStarter is a WordPress service station theme that I am currently building. It uses the Bootstrap/WordPress starter theme found here: https://github.com/them-es/themes-starter-bootstrap.

The theme utilizes the Advanced Custom Fields plugin and its Flexible Content Fields to create re-usable html templates for the various sections in the website. The page-builder.php template part can be used to build most single and multi-column sections of the site by pulling any number of partial templates and using a section_id (which is really a class) to scope the SASS styling.

