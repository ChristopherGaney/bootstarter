<?php 

/*
Infinite Ticker Template Part
*/


$shortcode = get_sub_field('shortcode');

?>

<section class="infinite_ticker">
	<div class="container">
		<div class="ticker_wrap">
			<?php echo do_shortcode($shortcode); ?>
		</div>
	</div>
</section>
