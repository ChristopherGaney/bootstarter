<?php 

/*
Page Builder Template Part
*/

$section_id = get_sub_field('section_id');
$columns = get_sub_field('columns');
$reverse_mobile = get_sub_field('reverse_mobile');

$sid = '';
if(!empty($section_id)) {
    $sid = ' ' . $section_id;
}

$reverse = '';
if($reverse_mobile) {
    $reverse = ' reverse_desk';
}


    $background_small = get_sub_field('background_small');
    $background_large = get_sub_field('background_large');
    
    $backrow = '';
    if (!empty($background_small)) { 
        $backrow = 'data-back="' . $background_small['url'] . '|' . $background_large['url'] . '"';
    }
    else if(!empty($background_large)) {
        $backrow = 'style="background: url(' . $background_large['url'] . ')"';
    }
?>

<section class="page_builder<?php echo $sid; ?>">
    <div class="container" <?php echo $backrow; ?>>
    
        <div class="row<?php echo $reverse; ?>">

            <?php 

                $field_name = "content_boxes";
    
                
                $ind = 1;
                if( have_rows($field_name) ):

                    while( have_rows($field_name) ): the_row();

                    $background = get_sub_field('background');
                    $back = '';

                    switch ($background) {
                        case 'image':
                            $image_small = get_sub_field('image_small');
                            $image_large = get_sub_field('image_large');
                            
                           
                            if (!empty($image_small)) { 
                                $back = 'data-back="' . $image_small['url'] . '|' . $image_large['url'] . '"';
                            }
                            else {
                                $back = 'style="background: url(' . $image_large['url'] . ')"';
                            }
                            break;

                        case 'color':
                            $background_color = get_sub_field('background_color');
                            $back = 'style="background: ' . $background_color . '"';
                            break;
                        case 'none':
                            break;
                        default:
                            break;
                    }

                    $box_id = get_sub_field('box_id');

                    $box_class = '';
                    if(!empty($box_id)) {
                        $box_class = ' ' . $box_id;
                    }
                    

            ?>

            <div class="<?php echo $columns; ?> cell-<?php echo $ind; ?><?php echo $box_class; ?>" <?php echo $back; ?>>
                    <div class="inside_box<?php if($sid == ' service_hero' && $ind == 2) { echo ' slider_text'; } ?>">
                    <?php

                        if( have_rows('content_box') ):
                    
                            while ( have_rows('content_box') ) : the_row();
                                
                                if( get_row_layout() == 'headline_block' ):
                 
                                    get_template_part( 'partials/headline-block' );

                                elseif( get_row_layout() == 'paragraph_block' ):
                 
                                    get_template_part( 'partials/paragraph-block' );

                                elseif( get_row_layout() == 'image_block' ):
                 
                                    get_template_part( 'partials/image-block' );

                                elseif( get_row_layout() == 'list_block' ):
                 
                                    get_template_part( 'partials/list-block' );

                                elseif( get_row_layout() == 'inline_list_with_icons' ):
                 
                                    get_template_part( 'partials/inline-list-with-icons' );

                                elseif( get_row_layout() == 'button_block' ):
                 
                                    get_template_part( 'partials/button-block' );

                                 elseif( get_row_layout() == 'video_block' ):
                 
                                    get_template_part( 'partials/video-block' );

                                elseif( get_row_layout() == 'price_block' ):
                 
                                    get_template_part( 'partials/price-block' );

                                endif;

                            endwhile;

                        endif;

                    ?>
                    </div>
              
            </div>
                
            <?php $ind++;

            endwhile; endif; ?>
            
        </div>
    </div>
</section>