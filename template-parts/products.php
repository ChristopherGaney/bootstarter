<?php 

/*
Page Builder Template Part
*/

?>


<section class="product_section" id="product_section">
	<div class="container">
		<ul class="products row">
			<?php
				$per_page = -1;
				$args = array(
					'post_type' => 'product',
					'posts_per_page' => $per_page
					);

				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) {
					while ( $loop->have_posts() ) : $loop->the_post();
						global $product;
						//print_r($product);
						wc_get_template_part( 'content', 'product' );
					endwhile;
				} else {
					echo __( 'No products found' );
				}
				wp_reset_postdata();
			?>
		</ul>
	</div>
</section>