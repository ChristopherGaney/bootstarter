<?php 

/*
Image Block Template Partial
*/

$image_small = get_sub_field('image_small');
$image_large = get_sub_field('image_large');
$image_alignment = get_sub_field('image_alignment');
$alt = get_sub_field('alt_text');


if (!empty($image_small)) { 
    $back = 'data-back="' . $image_small['url'] . '|' . $image_large['url'] . '"';
}
else {
    $back = 'src="' . $image_large['url'] . '"';
}


?>

<div class="image_block" style="text-align: <?php echo $image_alignment; ?>;">
	<img alt="<?php echo $alt; ?>" <?php echo $back; ?>>
</div>
