<?php 

/*
Gas Prices Template Partial
*/

$reg_name = get_sub_field('reg_name');
$reg_price = get_sub_field('reg_price');

$plus_name = get_sub_field('plus_name');
$plus_price = get_sub_field('plus_price');

$sup_name = get_sub_field('sup_name');
$sup_price = get_sub_field('sup_price');

$diesel_name = get_sub_field('diesel_name');
$diesel_price = get_sub_field('diesel_price');

?>

<div class="price_block">
	<div class="inner_wrap">
		<div class="content_wrap">
			<div class="title_wrap">
				<h2 class="price_title">Fuel Prices</h2>
			</div>
			<div class="price_wrap">
				<div class="price_bar">
					<div class="price_name">
						<?php echo $reg_name; ?>
					</div>
					<div class="price_val">
						$<?php echo $reg_price; ?>
					</div>
				</div>
				<div class="price_bar">
					<div class="price_name">
						<?php echo $plus_name; ?>
					</div>
					<div class="price_val">
						$<?php echo $plus_price; ?>
					</div>
				</div>
				<div class="price_bar">
					<div class="price_name">
						<?php echo $sup_name; ?>
					</div>
					<div class="price_val">
						$<?php echo $sup_price; ?>
					</div>
				</div>
				<div class="price_bar">
					<div class="price_name">
						<?php echo $diesel_name; ?>
					</div>
					<div class="price_val">
						$<?php echo $diesel_price; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>